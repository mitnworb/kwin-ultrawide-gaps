# Installation

```bash
$ git clone git@gitlab.com:mitnworb/kwin-ultrawide-gaps.git
$ cd kwin-ultrawide-gaps
$ plasmapkg2 --type=kwinscript -i .
```
```bash
$ kwin_x11 --replace &

* or just restart the system
```
# Usage
Set the desired shortcuts under `System Settings > Shortcuts > KWin`.

| Shortcuts                                            |                                  |
| ---------------------------------------------------- | -------------------------------- |
| <kbd>ctrl</kbd> + <kbd>Meta</kbd> + <kbd>Up</kbd>    | <kbd>Center Window</kbd>         |
| <kbd>ctrl</kbd> + <kbd>Meta</kbd> + <kbd>Left</kbd>  | <kbd>Move Window to Left</kbd>   |
| <kbd>ctrl</kbd> + <kbd>Meta</kbd> + <kbd>Right</kbd> | <kbd>Move Window to Right</kbd>  |

